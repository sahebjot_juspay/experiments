const exec = require('child_process').exec
const args = process.argv.slice(2)
const fs = require('fs')

let module_name = args[0]

const replaceModuleValues = () => {
  let fileContent = fs.readFileSync(`./${module_name}/Main.purs`, 'utf8')
  fileContent = fileContent.replace(`module Main where`, `module ${module_name} where`)
  fs.writeFileSync(`./${module_name}/Main.purs`, fileContent, {'encoding':'utf8'})
}

let create = exec(`cp -R "./.tmp"  ./${module_name}`, replaceModuleValues)

