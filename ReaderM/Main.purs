module ReaderM where

import Prelude
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Console (CONSOLE, log)
import Data.Maybe
import Control.Monad.Reader
import Partial.Unsafe (unsafePartial)

type User = String

type Permissions = { test :: String }

hasPermission :: String -> Permissions -> Boolean
hasPermission s per = case per.test of
                           "123" -> true
                           _     -> false

createUser :: Reader Permissions (Maybe User)
createUser = do
  permissions <- ask
  if hasPermission "admin" permissions
     then pure $ Just "u got permission"
     else pure Nothing

modifyPermission :: String -> Reader Permissions (Maybe User) -> Reader Permissions (Maybe User)
modifyPermission s pers = local (\x -> {test: x.test <> s}) pers

main :: forall e. Eff (console :: CONSOLE | e) Unit
main = do
  let config = {test: "123"}
  case runReader createUser config of
       Just user -> log $ "user " <> user
       Nothing   -> log "not allowed to create user"
  log "test"
