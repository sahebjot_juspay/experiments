const exec = require('child_process').exec
let args = process.argv.slice(2)
let src_path = args[0]

let npm = exec(`npm config set experiments:src_path=${src_path} && npm run module-watch-build`, () => {
})

let toPrint = ''
let newLineIndex = -1

const handleConsole = (d) => {
  newLineIndex = d.indexOf('\n')
  if(newLineIndex != -1) {
    toPrint += d.substr(0, newLineIndex)
    console.log(toPrint)
    toPrint = d.substr(newLineIndex, d.length)
  } else {
    toPrint += d
  }
}

npm.stdout.on("data", handleConsole)
npm.stderr.on("data", handleConsole)
