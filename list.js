const fs = require('fs')
const path = require('path')

let ignoreDirs = ['node_modules', 'output', 'test', 'bower_components']
const isPscProject = f => fs.lstatSync(path.join(process.cwd(), f)).isDirectory() && !(ignoreDirs.indexOf(f) != -1) && f[0] != '.'
fs.readdirSync(process.cwd()).filter(isPscProject).map(f => console.log(f))
