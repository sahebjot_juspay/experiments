module FileTree where

import Prelude
import Control.Monad.Eff.Console (CONSOLE, log)
import Control.Monad.Eff.Class (liftEff)
import Control.Monad.Eff.Exception (EXCEPTION)
import Node.FS.Aff (FS, readdir, stat)
import Node.FS.Stats (isDirectory)
import Data.Array (head, snoc)
import Data.Maybe (fromJust)
import Node.Process (cwd)
import Partial.Unsafe (unsafePartial)
import Control.Monad.Aff (launchAff, Aff)
import Node.Path (FilePath, normalize, basename)

type Directory = String

type FileState =
  {
    fileName :: String
  , filePath :: FilePath
  }

type DirectoryState =
  { fileName :: String
  , fileList :: Array FileTree
  , directoryPath :: FilePath
  }

data FileTree = File String | Directory (Array FileTree)

createFileTree :: FilePath -> Array FileTree -> Aff _ FileTree
createFileTree cwd directoryArr = do
  stat <- stat cwd
  if isDirectory stat
    then do
      files <- readdir cwd
      fileTree <- (\file -> createFileTree file directoryArr) <$> files
      Directory fileTree
    else do
      snoc directoryArr File (basename cwd)
      File (basename cwd)



--getDirectoryList :: FilePath -> Array FileTree
--getDirectoryList 
{-
treePath :: forall e. FilePath -> Array Directory -> Aff (console :: CONSOLE, fs :: FS, exception :: EXCEPTION | e) Array FilePath
treePath cwd ignoredDirectories = unsafePartial $ do
  fileStat <- stat cwd
  if isDirectory fileStat
     then treePath cwd
     else 
  files <- readdir cwd
  (\file -> do
    if isDirectory file
       then )
  liftEff $ log $ "test" <> (fromJust $ head files)
  pure unit
  -}
--main :: forall e. Eff (console :: CONSOLE, fs :: FS, exception :: EXCEPTION | e) Unit
main = void $ launchAff do
  --_ <- (\filePath -> treePath (normalize $ filePath <> "../" ) ["bower_components", "node_modules", "output"]) <$> (liftEff cwd)
  liftEff $ log "Hello World!"
