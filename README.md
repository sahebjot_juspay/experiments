### install
`npm i`
`bower i`

## use
### create an experiment
`node create <experiment_name>` (this will create a module)

### start
`node start <experiment_name>` (this will run the experiment and watch it)

### watch
`node watch <experiment_name>` (this will build and watch for that experiment change)

### list
`node list` (list's all the experiments)

### open
`node open <experiment_name>` (opens the main file for that experiment)

### remove
`node remove <experiment_name>` (removes that experiment)

### build
`node build <experiment_name>` (builds that experiment)
