const exec = require('child_process').exec
let args = process.argv.slice(2)
let src_path = args[0]

let rm = exec(`rm -rf ${src_path}`, () => {
})

rm.stdout.on("data", (d) => {
  console.log(d)
})

rm.stderr.on("data", (e) => {
  console.error(e)
})
