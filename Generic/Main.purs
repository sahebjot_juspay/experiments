module Generic where

import Prelude
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Console (CONSOLE, log, logShow)
import Data.Maybe (Maybe(..))
import Data.Generic (class Generic, gShow, fromSpine, toSpine)
import Abc as A

data Foo
 = Foo Number String
 | Baz {a :: Maybe String, bq :: Number} String

derive instance genericFoo :: Generic Foo
{-
instance showFoo :: Show Foo where
  show (Foo n s) = show n <> " " <> show s
  show (Baz _ s) = show s
-}

instance showFoo :: Show Foo where
  show = gShow


toFrom :: forall a. Generic a => a -> Maybe a
toFrom x = fromSpine (toSpine x)

main :: forall e. Eff (console :: CONSOLE | e) Unit
main = do
  let f = Foo 1.0 ""
  logShow $ toSpine f
  logShow $ A.abc 1 2
