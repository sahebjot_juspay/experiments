module Abc where

import Prelude

abc :: Int -> Int -> Int
abc a b = a + b
