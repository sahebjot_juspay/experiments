module ExceptT where

import Prelude
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Console (CONSOLE, log)
import Data.Either (Either(..), either)
import Control.Monad.Except.Trans (ExceptT(..), runExceptT, except)


myFunction :: forall m. Monad m => Int -> Int -> ExceptT String m Int
myFunction a b = do
  except if(a > b)
     then Right $ a + b
     else (Left "failure!!!!")

main :: forall e. Eff (console :: CONSOLE | e) Unit
main = do
  res <- runExceptT $ myFunction 15 2
  either (\(x :: String) -> log $ show x) (\y -> log $ show y) res
