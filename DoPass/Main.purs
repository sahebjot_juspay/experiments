module DoPass where

import Prelude
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Console (CONSOLE, log, logShow)

adder :: forall e. Eff (|e) Number -> Eff (|e) Number -> Eff (|e) Number
adder e1 e2 = do
  a <- e1
  b <- e2
  pure (a + b)

adder2 :: forall e. Eff (|e) Number -> Eff (|e) Number -> Eff (|e) Number
adder2 e1 e2 = (+) <$> e1 <*> e2

main :: forall e. Eff (console :: CONSOLE | e) Unit
main = do
  n <- adder (do
               let a = 1.0
               pure (5.0 + a)) (do pure 5.0)
  n2 <- adder2 (do
                 let a = 1.0
                 pure (4.0 + a)) (do pure 4.0)

  logShow $ n + n2
